Problem statement 

From a given set of latitude and longitude create a continuos path.

Solution

1. Plot the graph of current dataset.
2. Apply regression to find a feasible curve to represent the dataset.
3. Calculate accuracy of the model.

Scope : [Not implemented due to lack of understanding]
1. Use other techniques to split training and testing set 
{kfold, StratifiedKfold, Repeated}
2. Use lasso and ridge regression to create the model and calculate the score.
3. Different metrics to calculate the accuracy
4. Validate whether hyper tuning is relevant 



