from math import cos, asin, sqrt, pi
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures


class ModelProcess(object):

    def __init__(self) -> None:
        self.X = None
        self.Y = None
        self.create_path()
        self.filter_road()
        super().__init__()

    def prepare_data(self):
        self.df=pd.read_csv('latitude_longitude_details.csv')
        self.df = self.df[self.df.latitude != 12.0075892]
        self.df = self.df[self.df.longitude < 76.40]
        self.df.drop_duplicates(inplace=True)
        self.X = self.df.drop(columns=["longitude"])
        self.y = self.df["longitude"]

    def linear_regression(self, *args, **kwargs):
        X, y = self.X, self.y
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
        return LinearRegression().fit(X_train, y_train)

    def polynomial(self, *args, **kwargs):
        self.prepare_data()
        X, y = self.X, self.y
        poly = PolynomialFeatures(degree=4)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
        poly_df = pd.DataFrame(X_train)
        x_train_poly = poly.fit_transform(poly_df)
        self.X = poly.fit_transform(pd.DataFrame(X))
        return LinearRegression().fit(x_train_poly, y_train)

    def create_path(self):
        model = self.polynomial()
        self.df = self.df.assign(longitude = lambda x: model.predict(self.X))
        self.df['latitude_1'] = self.df.latitude.shift(-1)
        self.df['longitude_1'] = self.df.latitude.shift(-1)
        self.df['distance'] = self.df.apply(lambda x : self.distance(x.latitude_1, x.longitude_1, x.latitude, x.longitude), axis=1).shift(1)
        self.df.fillna(0, inplace=True)
        self.df.to_csv('output.csv', index = True)
        self.df.fillna(0, inplace=True)
        self.fill_terrain()

    def distance(self, lat1, lon1, lat2, lon2):
        p = pi/180
        a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p) * cos(lat2*p) * (1-cos((lon2-lon1)*p))/2
        return 12742 * asin(sqrt(a))

    def fill_terrain(self):
        self.df['terrain_scale'] = self.df.apply(lambda x: (3 * (x.distance - self.df.distance.nsmallest(2).iloc[-1]) / (self.df.distance.max() - self.df.distance.nsmallest(2).iloc[-1])), axis=1)
        self.df['terrain'] = self.df.apply(self.terrain, axis=1)

    def filter_road(self):
        return self.df[self.df['terrain'].str.contains('road')]

    def terrain(row):
        row = row.terrain_scale
        if row < 0.25:
            return 'boundary wall'
        elif row < 1:
            return 'road'
        elif row < 2.25:
            return 'river side'
        else:
            return 'civil station, road'

if __name__ == '__main__':
    model = ModelProcess()